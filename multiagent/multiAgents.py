# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util,math

from game import Agent

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"

        food_dists = [util.manhattanDistance(newPos, food) for food in newFood.asList()]
        ghost_dists = [util.manhattanDistance(newPos, ghost) for ghost in successorGameState.getGhostPositions()]

        min_ghost_dist = min(ghost_dists)
        
        if len(food_dists):
            min_food_dist = min(food_dists)
        else:
            min_food_dist = 0
    
        if currentGameState.isLose() or newPos == currentGameState.getPacmanPosition() or min(ghost_dists) < 2: 
            return -float("inf")
        elif currentGameState.isWin():
            return float("inf")

        return 1*currentGameState.getScore() -1.5*min_food_dist -2*(1./min_ghost_dist) -20*len(food_dists)


def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """
    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        #Funció inicial per començar l'algorisme
        def minimax(state, depth):
            action, value = max_value(state, depth) #El primer torn és del pacman, volem maximitzar
            return action
        
        #Helper function pel max value, el torn de pacman
        def max_value(state, depth):
            if depth == 0 or state.isWin() or state.isLose():  #Si és un estat final, retornem directament el valor
                return (None, self.evaluationFunction(state))
            
            valid_action = state.getLegalActions(0)     #Agafem les possibles accions de pacman
            action_evaluation = []
            for action in valid_action: #Per a cada acció de pacman cridem a min_value amb l'estat successor
                action_evaluation.append( (action, min_value(state.generateSuccessor(0, action), depth, 1)[1]) ) #Guardem la parella de l'acció i el min_value
                
            return max(action_evaluation, key=lambda x: x[1]) #Agafem el parell amb el segon element (el min_value) mínim
        
        #Helper function pel min_value
        def min_value(state, depth, player):
            if depth == 0 or state.isWin() or state.isLose(): #Si és un estat final, retornem directament el valor
                return (None, self.evaluationFunction(state))

            valid_action = state.getLegalActions(player) #Agafem les possibles accions del fantasma corresponent

            if player == state.getNumAgents()-1: #Si és l'ultim fantasma, haurem de cridar el max value pel pacman i disminuir la depth
                #Per a cada acció del fantasma, generem l'estat i cridem max_value pel pacman amb depth-1 
                action_evaluation = [(action, max_value(state.generateSuccessor(player, action), depth-1)[1]) for action in valid_action]
            else: #Si no és l'últim, hem de cridar min_value pel següent fantasma SENSE DISMINUIR LA DEPTH
                #Per a cada acció del fantasma, generem l'estat i cridem min_value pel següent (player+1)
                action_evaluation = [(action, min_value(state.generateSuccessor(player, action), depth, player+1)[1]) for action in valid_action]
            return min(action_evaluation, key=lambda x: x[1]) #Agafem la parella (acció, valor) amb valor mínim
        
        return minimax(gameState, self.depth)

#-------------------------------------------------------------------------------------
# IMPLEMENTACIÖ AMB UNA SOLA FUNCIÓ RECURSIVA
#        def minimax2(state, depth, player):
#            if depth == 0 or state.isWin() or state.isLose():
#                return (None, self.evaluationFunction(state))
#            
#            player_index = player%state.getNumAgents()
#            valid_action = state.getLegalActions(player_index)
#
#            if player_index == 0:
#                action_evaluation = [(action, minimax2(state.generateSuccessor(player_index, action), depth, player_index+1)[1]) for action in valid_action]
#                return max(action_evaluation, key=lambda x: x[1]) #Agafem el parell amb el primer element mínim
#            else:
#                next_depth = depth
#                if player_index == state.getNumAgents()-1:
#                    next_depth -= 1
#                
#                action_evaluation = [(action, minimax2(state.generateSuccessor(player_index, action), next_depth, player_index+1)[1]) for action in valid_action]
#                return min(action_evaluation, key=lambda x: x[1])
#
#        return minimax2(gameState, self.depth, 0)[0]
#-------------------------------------------------------------------------------------
            




        


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        def alphabeta(state, depth, a, b):
            return max_value(state, depth, a, b)[0]
            
        def max_value(state, depth, a, b):
            if depth == 0 or state.isWin() or state.isLose():
                return (None, self.evaluationFunction(state))
            
            valid_action = state.getLegalActions(0)
            action_utility = (None,-float('inf'))
            
            for action in valid_action:
                action_utility = max(action_utility, (action, min_value(state.generateSuccessor(0, action), depth, 1, a, b)[1]), key=lambda x: x[1])
                if action_utility[1] > b:
                    return action_utility
                a = max(a, action_utility[1])
            return action_utility
        
        def min_value(state, depth, player, a, b):
            if depth == 0 or state.isWin() or state.isLose():
                return (None, self.evaluationFunction(state))
            
            valid_action = state.getLegalActions(player)
            action_utility = (None,float('inf'))
            
            if player == state.getNumAgents() -1:
                for action in valid_action:
                    action_utility = min(action_utility, (action, max_value(state.generateSuccessor(player, action), depth-1, a, b)[1]), key=lambda x: x[1])
                    if action_utility[1] < a:
                        return action_utility
                    b = min(b, action_utility[1])
                return action_utility
            else: 
                for action in valid_action:
                    action_utility = min(action_utility, (action, min_value(state.generateSuccessor(player, action), depth, player+1, a, b)[1]), key=lambda x: x[1])
                    if action_utility[1] < a:
                        return action_utility
                    b = min(b, action_utility[1])
                return action_utility
        
        return alphabeta(gameState, self.depth, -float('inf'), float('inf'))
    
    
#-------------------------------------------------------------------------------------
# IMPLEMENTACIÖ AMB UNA SOLA FUNCIÓ RECURSIVA
#        def minimax(state, depth, player, a, b):
#            if depth == 0 or state.isWin() or state.isLose():
#                return (None, self.evaluationFunction(state))
#            
#            player_index = player%state.getNumAgents()
#            valid_action = state.getLegalActions(player_index)
#
#            if player_index == 0:
#                action_utility = (None,-float('inf'))
#                for action in valid_action:
#                    action_utility = max(action_utility, (action, minimax(state.generateSuccessor(player_index, action), depth, player_index+1, a, b)[1]), key=lambda x: x[1])
#                    if action_utility[1] > b:
#                        return action_utility
#                    a = max(a, action_utility[1])
#                return action_utility
#
#            else:
#                next_depth = depth
#                if player_index == state.getNumAgents()-1:
#                    next_depth -= 1
#                
#                action_utility = (None,float('inf'))
#                for action in valid_action:
#                    action_utility = min(action_utility, (action, minimax(state.generateSuccessor(player_index, action), next_depth, player_index+1, a, b)[1]), key=lambda x: x[1])
#                    if action_utility[1] < a:
#                        return action_utility
#                    b = min(b, action_utility[1])
#                return action_utility
#        return minimax(gameState, self.depth, 0, -float('inf'), float('inf'))[0]
#-------------------------------------------------------------------------------------


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        def expectimax(state, depth):
            action, value = max_value(state, depth)
            print(value)
            return action
        

        def max_value(state, depth):
            if depth == 0 or state.isWin() or state.isLose():
                return (None, self.evaluationFunction(state))
            
            valid_action = state.getLegalActions(0)
            action_evaluation = [(action, expected_value(state.generateSuccessor(0, action), depth, 1)[1]) for action in valid_action]
            return max(action_evaluation, key=lambda x: x[1]) #Agafem el parell amb el primer element mínim
        
        def expected_value(state, depth, player):
            if depth == 0 or state.isWin() or state.isLose():
                return (None, self.evaluationFunction(state))

            valid_action = state.getLegalActions(player)

            if player == state.getNumAgents()-1:
                evaluation = [max_value(state.generateSuccessor(player, action), depth-1)[1] for action in valid_action]
            else:
                evaluation = [expected_value(state.generateSuccessor(player, action), depth, player+1)[1] for action in valid_action]
            return (None, 1/len(valid_action) * sum(evaluation))
        
        return expectimax(gameState, self.depth)
#-------------------------------------------------------------------------------------
# IMPLEMENTACIÖ AMB UNA SOLA FUNCIÓ RECURSIVA
#        def expectimax(state, depth, player):
#            if depth == 0 or state.isWin() or state.isLose():
#                return (None, self.evaluationFunction(state))
#            
#            player_index = player%state.getNumAgents()
#            valid_action = state.getLegalActions(player_index)
#
#            if player_index == 0:
#                action_evaluation = [(action, expectimax(state.generateSuccessor(player_index, action), depth, player_index+1)[1]) for action in valid_action]
#                return max(action_evaluation, key=lambda x: x[1]) #Agafem el parell amb el primer element mínim
#            else:
#                next_depth = depth
#                if player_index == state.getNumAgents()-1:
#                    next_depth -= 1
#                
#                evaluation = [expectimax(state.generateSuccessor(player_index, action), next_depth, player_index+1)[1] for action in valid_action]
#                return (None, 1/len(valid_action) * sum(evaluation))
#        return expectimax(gameState, self.depth, 0)[0]
#-------------------------------------------------------------------------------------


def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"


    pos = currentGameState.getPacmanPosition()
    food = currentGameState.getFood()
    capsules = currentGameState.getCapsules()
    food_dists = [util.manhattanDistance(pos, food) for food in food.asList()]
    capsule_dist = [util.manhattanDistance(pos, capsule) for capsule in capsules]
    ghost_dists = [(ghost,util.manhattanDistance(pos, ghost.getPosition())) for ghost in currentGameState.getGhostStates()]
    food_pos = food.asList()
    
    #Calcula la distància màxima entre dos menjars
    distance_between_food = [0]
    for i in range(len(food_pos)):
        for j in range(i+1, len(food_pos)):
            distance_between_food.append(util.manhattanDistance(food_pos[i], food_pos[j]))
    max_food = max(distance_between_food)

    #Mirem si hi ha algun fantasma a esquivar o menjar
    min_ghost = min(ghost_dists, key=lambda x: x[1]) #Agafem el més proper
    #if min_ghost[1] <=2: #Si sestà a una casella de distància
        #if min_ghost[0].scaredTimer > 0: #Si el podem menjar
            #return 200000
        #else:
            #return -200000
    
    #closest_dead_ghost = min([util.manhattanDistance(pos, ghost.getPosition()) for ghost in ghost_states if ghost.scaredTimer != 0])
   # if min(ghost_dists):
   #     min_ghost_dist = 1#-2/min(ghost_dists)
    #else:
     #   min_ghost_dist = -500

    if food.count() == 0:
        print("Aquí puc acabar")
        return 10000000
    
    if food.count() > 0:

        min_food_dist = min(food_dists)
    else:
        return 200000
    
    if min_food_dist == 0:
        min_food_dist = -500
    
    if len(capsules):
        min_capsule = -2/min(capsule_dist)
    else:
        min_capsule = 100
    
    #print("hey2")
    #value = 1*currentGameState.getScore()+100*1/min_food_dist+0/math.exp(max_food-5)+100*1/len(food_pos)+0*min_capsule
    value = 1*currentGameState.getScore() +\
        -1*minimum_food_dist(currentGameState)+ \
         0*len(currentGameState.getCapsules()) +\
         -0*food.count()
    print(pos, value)
    return value

    """
    if currentGameState.isWin():
        return float("inf")
    if min_ghost_dist:
        return 1*currentGameState.getScore() -1.5*min_food_dist -2*(1./min_ghost_dist) -20*len(food_dists)
    return 1*currentGameState.getScore() -1.5*min_food_dist -20*len(food_dists)
    """


def minimum_food_dist(currentGameState):
    "BFS to find closest food"
    pos = currentGameState.getPacmanPosition()
    food = currentGameState.getFood()
    if food.count() == 0:
        return 200000
    if food[pos[0]][pos[1]]:
        return 0

    walls = currentGameState.getWalls()
    visited = walls.copy()
    queue = util.Queue()
    visited[pos[0]][pos[1]] = 0
    queue.push(pos)
    while not queue.isEmpty():
        x,y = queue.pop()
        dist = visited[x][y] + 1
        if food[x][y]:
            return dist
        for v in [(0, 1) , (1, 0) , (0 , -1) , (-1 , 0)]:
            xn = x + v[0]
            yn = y + v[1]
            if visited[xn][yn] == False:
                visited[xn][yn] = dist
                queue.push((xn, yn))

def breadthFirstSearch(currentGameState):
    """Search the shallowest nodes in the search tree first."""
    pos = currentGameState.getPacmanPosition()
    food = currentGameState.getFood()
    if food.count() == 0:
        return float('inf')                                         #Si no queda menjar, ja hem guanyat


    walls = currentGameState.getWalls()
    queue = util.Queue()
    cost ={}                                                        #Guardem el conjunt dels nodes visitats juntament amb el cost actual
    cost[pos]=0                                                     #Posem l'estat inicial a distància 0 (visitat)
    queue.push((pos, 0))                                            #Posem l'estat a la cua
    while not queue.isEmpty():                                      #Mentre no haguem recorregut tot el graf
        pos, current_cost = queue.pop()                             #treiem el següent
        if food[pos[0]][pos[1]]:                                    #Si trobem menjar, ja hem acabat
            return current_cost
        for d in [(0, 1) , (1, 0) , (0 , -1) , (-1 , 0)]:           #Busquem els successors
            dx, dy = pos[0] + d[0], pos[1] + d[1]                   #Calculem les noves coordenades
            if walls[dx][dy] == False:
                new_cost = current_cost + 1                         #Calculem el nou cost
                if (dx,dy) not in cost or new_cost < cost[(dx,dy)]: #Afegim a la cua si no s'ha trobat o el camí que hem trobat és millor
                    cost[(dx,dy)] = new_cost                        #El marquem com a visitat i actualitzem el cost si és necessari
                    queue.push(((dx,dy), new_cost))                 #Posem a la cua.





class CustomAgent(ExpectimaxAgent):
    """
    Custom class to experiment and find better eval values
    """
    

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2', a=0, b=0, c=0, d=0, e=0, f=0):
        self.depth = int(depth)
        self.coef_score = int(a)
        self.coef_food_dist = int(b)
        self.coef_dead_ghost = int(c)
        self.coef_active_ghost = int(d)
        self.coef_nfood = int(e)
        self.coef_npill = int(f)
        self.evaluationFunction = self.evaluation
    
    def evaluation(self, currentGameState):
        return customEvaluation(currentGameState, self.coef_score, self.coef_food_dist, self.coef_dead_ghost, self.coef_active_ghost, self.coef_nfood, self.coef_npill)
    
   



def customEvaluation(currentGameState, coef_score, coef_food_dist, coef_dead_ghost, coef_active_ghost, coef_nfood, coef_npill):
    pos = currentGameState.getPacmanPosition()
    food = currentGameState.getFood()
    ghost_states = currentGameState.getGhostStates()
    food_dists = [util.manhattanDistance(pos, food) for food in food.asList()]+[0]
    ghost_dists = [util.manhattanDistance(pos, ghost) for ghost in currentGameState.getGhostPositions()]+[0]

    closest_dead_ghost = min([util.manhattanDistance(pos, ghost.getPosition()) for ghost in ghost_states if ghost.scaredTimer != 0] + [0])


    return  coef_score * currentGameState.getScore() + coef_food_dist * min(food_dists) + coef_dead_ghost* closest_dead_ghost + coef_active_ghost*min(ghost_dists)+ coef_nfood*len(food_dists) + coef_npill*len(currentGameState.getCapsules())



# Abbreviation
better = betterEvaluationFunction
