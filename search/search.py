# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    """
    stack = util.Stack()
    visited = set()
    stack.push((problem.getStartState(), [], 0))
    while not stack.isEmpty():
        current_node, current_path, current_cost = stack.pop()
        visited.add(current_node)
        if problem.isGoalState(current_node):
            return current_path
        for child, direction, action_cost in problem.getSuccessors(current_node):
            new_cost = current_cost + action_cost
            if child not in visited:
                stack.push((child, current_path+[direction], new_cost))
    return []

        

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    queue = util.Queue()
    cost ={} #Guardem el conjunt dels nodes visitats juntament amb el cost actual
    cost[problem.getStartState()]=0 #Posem l'estat inicial a distància 0 (visitat)
    queue.push((problem.getStartState(), [], 0)) #Posem l'estat a la cua
    while not queue.isEmpty(): #Mentre no haguem recorregut tot el graf
        current_node, current_path, current_cost = queue.pop() #treiem el següent
        if problem.isGoalState(current_node): #Si és el goal, ja hem acabat
            return current_path
        for child, direction, action_cost in problem.getSuccessors(current_node): #Busquem els successors
            new_cost = current_cost + action_cost #Calculem el nou cost
            if child not in cost or new_cost < cost[child]: #Afegim a la cua si no s'ha trobat o el camí que hem trobat és millor
                cost[child] = new_cost #El marquem com a visitat i actualitzem el cost si és necessari
                queue.push((child, current_path+[direction], new_cost)) #Posem a la cua.
    return []

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    queue = util.PriorityQueue()
    cost ={} #Guardem el conjunt dels nodes visitats juntament amb el cost actual
    cost[problem.getStartState()]=0 #Posem l'estat inicial a distància 0 (visitat)
    queue.push((problem.getStartState(), [], 0),0) #Posem l'estat a la cua
    while not queue.isEmpty(): #Mentre no haguem recorregut tot el graf
        current_node, current_path, current_cost = queue.pop() #treiem el següent
        if problem.isGoalState(current_node): #Si és el goal, ja hem acabat
            return current_path
        for child, direction, action_cost in problem.getSuccessors(current_node): #Busquem els successors
            new_cost = current_cost + action_cost #Calculem el nou cost
            if child not in cost or new_cost < cost[child]: #Afegim a la cua si no s'ha trobat o el camí que hem trobat és millor
                cost[child] = new_cost #El marquem com a visitat i actualitzem el cost si és necessari
                queue.push((child, current_path+[direction], new_cost), new_cost) #Posem a la cua.

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    queue = util.PriorityQueue()
    cost ={} #Guardem el conjunt dels nodes visitats juntament amb el cost actual
    cost[problem.getStartState()]=0 #Posem l'estat inicial a distància 0 (visitat)
    queue.push((problem.getStartState(), [], 0),0) #Posem l'estat a la cua
    while not queue.isEmpty(): #Mentre no haguem recorregut tot el graf
        current_node, current_path, current_cost = queue.pop() #treiem el següent
        if problem.isGoalState(current_node): #Si és el goal, ja hem acabat
            return current_path
        for child, direction, action_cost in problem.getSuccessors(current_node): #Busquem els successors
            new_cost = current_cost + action_cost #Calculem el nou cost
            if child not in cost or new_cost < cost[child]: #Afegim a la cua si no s'ha trobat o el camí que hem trobat és millor
                cost[child] = new_cost #El marquem com a visitat i actualitzem el cost si és necessari
                queue.push((child, current_path+[direction], new_cost), new_cost + heuristic(child, problem)) #Posem a la cua amb la heurística

def aStarSearch2(problem, heuristic=nullHeuristic):
    queue = util.PriorityQueue()
    cost ={} #Això servirà per guardar el conjunt dels nodes visitats juntament amb el cost actual
    queue.push((problem.getStartState(), [], 0), 0)
    while not queue.isEmpty():
        current_node, current_path, current_cost = queue.pop()
        if problem.isGoalState(current_node):
            return current_path
        if current_node not in cost:
            cost[current_node] = current_cost
            for child, direction, action_cost in problem.getSuccessors(current_node):
                new_cost = current_cost + action_cost
                queue.push((child, current_path+[direction], new_cost), new_cost + heuristic(child, problem))
    return []
    



# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
